var gameStates = [ 'running', 'dead' ];
var currentState = undefined;

var playerSpeed;
var playerOrbitSpeed;
var playerOrbit = undefined;
var playerOrbitSpot;
var playerOrbitClockwise;
var playerFlyDirection;
var playerVelocity = new Phaser.Point(0, 0);
var playerCircle = new Phaser.Circle(0, 0, 24);

var leftWall;
var rightWall;
var water;
var waterRiseSpeed = 0.08;

var backgroundSprite = null;
var cameraSprite = null;
var witchSprite = null;
var leftWallTile = null;
var rightWallTile = null;

var trailX = [
  new Phaser.Circle(999, 999, 8),
  new Phaser.Circle(999, 999, 8),
  new Phaser.Circle(999, 999, 8),
  new Phaser.Circle(999, 999, 8),
];
var trailIndex = 0;
var trailTime = 0;

var planets;
var obstacles;

var onTouch = function()
{
  if (currentState == 'dead')
  {
    game.state.restart(true, false);
  }
  else
  {
    if (playerOrbit)
    {
      playerFlyDirection = Math.atan2(playerCircle.y - playerOrbit.y, playerCircle.x - playerOrbit.x) + (Math.PI * 0.5 * (playerOrbitClockwise ? 1 : -1));

      playerOrbit = undefined;
    }
  }
}

var resizeGame = function()
{
  var scale = 1;
  if (window.innerWidth > window.innerHeight)
  {
    scale = window.innerHeight / 1136;
  }
  else
  {
    scale = window.innerWidth / 640;
  }

  scale *= 0.95;

  game.scale.setUserScale(scale, scale);
  game.scale.refresh();
}

var setScaling = function()
{
  game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
  game.scale.pageAlignHorizontally = true;
  game.scale.pageAlignVertically = true;
  game.scale.setResizeCallback(this.resizeGame, this);
  resizeGame();
}

var preload = function()
{
  game.load.spritesheet('maple', 'asset/witch.png', 36, 44);
  game.load.image('wall', 'asset/wallTile.png');
};

var create = function()
{
  game.time.advancedTiming = true;

  playerCircle.x = -100;
  playerCircle.y = -100;
  playerFlyDirection = Math.PI / 4 + 0.02;
  playerOrbit = undefined;

  waterRiseSpeed = 0.08;
  playerSpeed = 0.75;
  playerOrbitSpeed = 1.5;

  trailIndex = 0;
  trailTime = 0;

  var newImage = game.add.bitmapData(640, 1136);
  var grd = newImage.context.createLinearGradient(320, 0, 320, 1136);
  grd.addColorStop(0, "#0000FF");
  grd.addColorStop(1, "#9999FF");
  newImage.context.fillStyle = grd;
  newImage.context.fillRect(0, 0, 640, 1136);
  backgroundSprite = game.add.sprite(0, 0, newImage);
  backgroundSprite.fixedToCamera = true;

  cameraSprite = game.add.sprite(0, 0, null);
  game.camera.bounds = null;
  game.camera.follow(cameraSprite, Phaser.Camera.FOLLOW_LOCKON);

  leftWallTile = game.add.tileSprite(-320, 1136 / -2, 32, 1136, 'wall');
  rightWallTile = game.add.tileSprite(320 - 32, 1136 / -2, 32, 1136, 'wall');

  witchSprite = game.add.sprite(0, 0, 'maple');
  witchSprite.anchor.x = 0.5;
  witchSprite.anchor.y = 0.75;

  game.input.onDown.add(onTouch, this);

  setScaling();

  leftWall = new Phaser.Rectangle(-320, 3200 / -2, 32, 3200);
  rightWall = new Phaser.Rectangle(320 - 32, 3200 / -2, 32, 3200);
  water = new Phaser.Rectangle(-320 + 32, 512, 576, 1200);

  // initialize an array of planets and link them in a circular list
  planets = [];
  obstacles = [];
  planets.push(new Phaser.Circle(0, 0, 100));
  obstacles.push(new Phaser.Circle(0, 0, 32));
  planets[planets.length - 1].obstacle = obstacles[obstacles.length - 1];
  for (var i = 1; i <= 20; i++)
  {
    planets.push(new Phaser.Circle(Math.random() * 320 - 160, -350 * i, Math.random() * 100 + 100));
    planets[planets.length - 1].prev = planets[planets.length - 2];

    obstacles.push(new Phaser.Circle(0, 0, 32));
    obstacles[obstacles.length - 1].spot = Math.random() * 2 * Math.PI;
    obstacles[obstacles.length - 1].spotTime = Math.random() * 400 * Math.PI;
    planets[planets.length - 1].obstacle = obstacles[obstacles.length - 1];
    planets[planets.length - 1].obstacle.x = (planets[planets.length - 1].x + planets[planets.length - 1].prev.x) / 2;
    planets[planets.length - 1].obstacle.y = (planets[planets.length - 1].y + planets[planets.length - 1].prev.y) / 2;
  }
  planets[0].prev = planets[planets.length - 1];
  planets[0].obstacle.x = Number.MAX_VALUE;
  planets[0].obstacle.y = Number.MAX_VALUE;

  currentState = gameStates[0];
}

var update = function()
{
  cameraSprite.y = Math.min(playerCircle.y, cameraSprite.y);

  // Update wall locations
  leftWall.y = 3200 / -2 + cameraSprite.y;
  rightWall.y = 3200 / -2 + cameraSprite.y;
  leftWallTile.y = 1136 / -2 + cameraSprite.y;
  leftWallTile.tilePosition.y = cameraSprite.y * -1;
  rightWallTile.y = 1136 / -2 + cameraSprite.y;
  rightWallTile.tilePosition.y = cameraSprite.y * -1;

  trailTime += game.time.elapsed;
  if (trailTime > 100)
  {
    trailTime = 0;

    trailX[trailIndex].x = playerCircle.x;
    trailX[trailIndex].y = playerCircle.y;
    trailX[trailIndex].radius = 8;

    trailIndex = (trailIndex + 1) % trailX.length;
  }

  trailX.forEach(function(trail)
  {
    trail.radius -= 0.01 * game.time.elapsed;
  });

  // Rise the water
  water.y -= waterRiseSpeed * game.time.elapsed * (Math.abs(playerCircle.y - water.top) < 586 ? 1 : 3);

  // If a planet has been passed offscreen, move it forward the front
  planets.forEach(function(planet)
  {
    if (planet.y > game.camera.y + 1136 * 1.5 + planet.radius)
    {
      planet.y -= planets.length * 350;
      planet.x = Math.random() * 320 - 160;
      planet.diameter = Math.random() * 100 + 100;
      planet.obstacle.x = (planet.x + planet.prev.x) / 2;
      planet.obstacle.y = (planet.y + planet.prev.y) / 2;
    }
  });

  // update each obstacle's location
  planets.forEach(function(planet)
  {
    var obstacleAngle = Math.atan2(planet.y - planet.prev.y, planet.x - planet.prev.x) + Math.PI / 2;

    planet.obstacle.x = (planet.x + planet.prev.x) / 2 + (Math.cos(obstacleAngle) * (128 * planet.obstacle.spot));
    planet.obstacle.y = (planet.y + planet.prev.y) / 2 + (Math.sin(obstacleAngle) * (128 * planet.obstacle.spot));

    planet.obstacle.spot = Math.sin(planet.obstacle.spotTime / 400);
    if (currentState != 'dead')
    {
      planet.obstacle.spotTime += game.time.elapsed;
    }
  });

  // player logic
  if (playerOrbit)
  {
    playerCircle.x = (Math.cos(playerOrbitSpot) * (playerOrbit.radius + playerCircle.radius)) + playerOrbit.x;
    playerCircle.y = (Math.sin(playerOrbitSpot) * (playerOrbit.radius + playerCircle.radius)) + playerOrbit.y;

    playerOrbitSpot += (playerOrbitClockwise ? playerOrbitSpeed : -playerOrbitSpeed) * game.time.elapsed / (2 * Math.PI * playerOrbit.radius);
  }
  else
  {
    playerCircle.x += Math.cos(playerFlyDirection) * playerSpeed * game.time.elapsed;
    playerCircle.y += Math.sin(playerFlyDirection) * playerSpeed * game.time.elapsed;

    planets.forEach(function (planet)
    {
      if (Phaser.Circle.intersects(playerCircle, planet))
      {
        playerOrbit = planet;
        playerOrbitSpot = Math.atan2(playerCircle.y -planet.y, playerCircle.x - planet.x);

        var step = new Phaser.Point(playerCircle.x + Math.cos(playerFlyDirection) * playerSpeed * game.time.elapsed, playerCircle.y + Math.sin(playerFlyDirection) * playerSpeed * game.time.elapsed);
        var stepOrbitSpot = Math.atan2(step.y -planet.y, step.x - planet.x);
        playerOrbitClockwise = stepOrbitSpot - playerOrbitSpot > 0;
      }
    });
  }

  // check the player against all objects
  var hitAnObstacle = false;
  obstacles.forEach(function (obstacle)
  {
    hitAnObstacle = Phaser.Circle.intersects(playerCircle, obstacle) || hitAnObstacle;
  });

  // end the game if the player dies
  if (hitAnObstacle || Phaser.Circle.intersectsRectangle(playerCircle, leftWall) || Phaser.Circle.intersectsRectangle(playerCircle, rightWall) || Phaser.Circle.intersectsRectangle(playerCircle, water))
  {
    currentState = gameStates[1];
    playerSpeed = 0;
    playerOrbitSpeed = 0;
    waterRiseSpeed = 0;
  }

  // view-related stuff
  witchSprite.x = playerCircle.x;
  witchSprite.y = playerCircle.y;

  if (playerOrbit)
  {
    witchSprite.rotation = Math.atan2(playerCircle.y - playerOrbit.y, playerCircle.x - playerOrbit.x) + (Math.PI * 0.5);
    witchSprite.scale.x = playerOrbitClockwise ? -1 : 1;
    witchSprite.scale.y = 1;
  }
  else
  {
    witchSprite.rotation = playerFlyDirection + Math.PI;
    witchSprite.scale.x = 1;
    witchSprite.scale.y = playerOrbitClockwise ? -1 : 1;
  }
};

var render = function()
{
  //game.debug.geom(playerCircle, 'red');

  planets.forEach(function(planet)
  {
    game.debug.geom(planet, '#00FF00');
    game.debug.geom(planet.obstacle, Math.random() < 0.5 ? 'red' : 'orange');
  });

  trailX.forEach(function(trail)
  {
    game.debug.geom(trail, '#FFFFFF');
  });

  game.debug.geom(water, 'blue');

  game.debug.text(game.time.fps || '--', 2, 14, "#00ff00");

  game.debug.text(~~(playerCircle.y * -0.0125) + 'm', 275, 96, 'white', 'bold 48pt Arial');

  if (currentState == 'dead')
  {
    game.debug.text('click to retry', 150, 1136 / 2 , 'white', 'bold 24pt Arial');
  }
};

var game = new Phaser.Game(640, 1136, Phaser.CANVAS, "", {create: create, preload: preload, update: update, render: render}, false, false);
